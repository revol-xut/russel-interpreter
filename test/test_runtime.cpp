//
// Created by revol-xut on 30.01.20.
//

#include <boost/test/unit_test.hpp>
#include <iostream>

#include "../include/runtime/runtime.hpp"
#include "../include/data_classes/data_class_interface.hpp"


BOOST_AUTO_TEST_CASE(test_runtime_1) { //NOLINT
    auto raw_program = std::shared_ptr<char[]>(new char[21]);
    raw_program[0] = 0xC; // declare scalar 0
    raw_program[1] = 0xC; // declare scalar 1
    raw_program[2] = 0xD; // declare vector 2
    raw_program[3] = 0x0; // Arg vector size
    raw_program[4] = 0xE; // declare matrix 3
    raw_program[5] = 0x1; // Arg Matrix size_x
    raw_program[6] = 0x0; // Arg Matrix size_y
    raw_program[7] = 0xD; // declare vector 4
    raw_program[8] = 0x1; // Arg vector size
    raw_program[9] = 0x14; // Add Operator
    raw_program[10] = 0x4; // declared Vector index 4
    raw_program[11] = 0x0; // declared Scalar index 0
    raw_program[12] = 0x4; // Puts result on index 4
    raw_program[13] = 0x1; // Print
    raw_program[14] = 0x4; // index 4
    raw_program[15] = 0x16; // multiplies
    raw_program[16] = 0x3; // index 3
    raw_program[17] = 0x4; // index 4
    raw_program[18] = 0x4; // pushon 4
    raw_program[19] = 0x1; //print
    raw_program[20] = 0x4; //index 4

    auto raw_data = std::shared_ptr<char[]>(new char[14 * sizeof(float)]);

    // The two scalar values
    datatypes::degrade(3, raw_data.get());
    datatypes::degrade(2, raw_data.get() + sizeof(float) * 1);

    // The vector
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 2);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 3);

    // the matrix
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 4);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 5);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 6);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 7);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 8);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 9);

    // The vector
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 10);
    datatypes::degrade(1, raw_data.get() + sizeof(float) * 11);
    datatypes::degrade(1, raw_data.get() + sizeof(float) * 12);
    datatypes::degrade(1, raw_data.get() + sizeof(float) * 13);

    auto runtime = std::make_shared<runtime::Runtime>(raw_data, raw_program, 21, sizeof(float) * 14);

    runtime->execute();


}

BOOST_AUTO_TEST_CASE(test_runtime_2) { //NOLINT
    auto raw_program = std::shared_ptr<char[]>(new char[10]);
    raw_program[0] = 0xC; // declare scalar 0
    raw_program[1] = 0xC; // declare scalar 1
    raw_program[2] = 0xC; // declare scalar 2
    raw_program[3] = 0xE; // declare matrix
    raw_program[4] = 0x0;
    raw_program[5] = 0x1;
    raw_program[6] = 0x16;
    raw_program[7] = 0x3;
    raw_program[8] = 0x2;
    raw_program[9] = 0x3;


    auto raw_data = std::shared_ptr<char[]>(new char[14 * sizeof(float)]);

    // The two scalar values
    datatypes::degrade(3.0, raw_data.get());
    datatypes::degrade(3.0, raw_data.get() + sizeof(float) * 1);
    datatypes::degrade(5.0, raw_data.get() + sizeof(float) * 2);

    // the matrix
    datatypes::degrade(1, raw_data.get() + sizeof(float) * 3);
    datatypes::degrade(2, raw_data.get() + sizeof(float) * 4);
    datatypes::degrade(3, raw_data.get() + sizeof(float) * 5);
    datatypes::degrade(4, raw_data.get() + sizeof(float) * 6);
    datatypes::degrade(5, raw_data.get() + sizeof(float) * 7);
    datatypes::degrade(6, raw_data.get() + sizeof(float) * 8);
    datatypes::degrade(7, raw_data.get() + sizeof(float) * 9);
    datatypes::degrade(8, raw_data.get() + sizeof(float) * 10);
    datatypes::degrade(9, raw_data.get() + sizeof(float) * 11);

    auto runtime = std::make_shared<runtime::Runtime>(raw_data, raw_program, 10, sizeof(float) * 14);

    runtime->execute();

    datatypes::AbstractType abstractType = runtime->getVariable(3);
    unsigned int size = abstractType.getSize();
    char *output = new char[size];

    abstractType.serializeObject(output);

    for (int i = 0; i < (abstractType.getSize() / sizeof(float)); i++) {
        std::cout << datatypes::castToValue(output + sizeof(float) * i) << std::endl;
    }
}
