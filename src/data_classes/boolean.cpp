//
// Created by einspaten on 19.02.20.
//

#include "../../include/data_classes/boolean.hpp"


datatypes::Boolean::Boolean() {
    value_ = false;
}

datatypes::Boolean::~Boolean() = default;

void datatypes::Boolean::parse(char *data_ptr) {
    value_ = static_cast<bool >(data_ptr);
}

void datatypes::Boolean::serialize(char *data_ptr) {
    *data_ptr = value_;
}

auto datatypes::Boolean::expected_size() -> unsigned int {
    return static_cast<unsigned int>(sizeof(bool)); // 1
}

void datatypes::Boolean::negate() {
    value_ = not value_;
}

auto datatypes::Boolean::getValue() -> bool {
    return value_;
}

void datatypes::Boolean::setData(bool value) {
    value_ = value;
}

void datatypes::Boolean::equals(Boolean *other_ptr, Boolean *push_on_ptr) {
    push_on_ptr->setData(other_ptr->getValue() == value_);
}