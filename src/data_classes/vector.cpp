//
// Created by revol-xut on 25.01.20.
//

#include "../../include/data_classes/vector.hpp"
#include <iostream>

datatypes::Vector::Vector(unsigned int size) {
    vector_ = std::make_shared<Eigen::VectorXf>(size);
    dimensions_ = size;
}

datatypes::Vector::~Vector() {}

unsigned int datatypes::Vector::expected_size() {
    return dimensions_ * sizeof(defaultType);
}

void datatypes::Vector::parse(char *data_ptr) {
    float value;
    for (int i = 0; i < dimensions_; i++) {
        // This can possible throw a segmentation fault
        value = datatypes::castToValue(data_ptr + i * sizeof(float));
        (*vector_)(i) = value;
    }
}

void datatypes::Vector::serialize(char *data_ptr) {
    for (int j = 0; j < dimensions_; j++) {
        datatypes::degrade((*vector_)(j), data_ptr + j * sizeof(float));
    }
}

void datatypes::Vector::add(Scalar *other_ptr, Vector *push_on_ptr) {
    // Adding a scalar value to a vector is mathematically incorrect this is logical work around
    auto temp = *vector_ + (Eigen::VectorXf::Ones(dimensions_) * other_ptr->getData());
    push_on_ptr->setData(temp);
    push_on_ptr->setDimensions(dimensions_);
}

void datatypes::Vector::add(Vector *other_ptr, Vector *push_on_ptr) {
    push_on_ptr->setData(*vector_ + other_ptr->getData());
    push_on_ptr->setDimensions(dimensions_);
}

void datatypes::Vector::mul(Scalar *other_ptr, Vector *push_on_ptr) {
    push_on_ptr->setData(*vector_ * other_ptr->getData());
    push_on_ptr->setDimensions(dimensions_);
}

void datatypes::Vector::dot_mul(Vector *other_ptr, Scalar *push_on_ptr) {
    if (other_ptr->getDimensions() != dimensions_) {
        throw std::runtime_error("Vector dimensions do not fit !");
    }
    push_on_ptr->setData((*vector_).dot(other_ptr->getData()));
}

void
datatypes::Vector::cross_mul(Vector *other_ptr, Vector *push_on_ptr) {
    if (other_ptr->getDimensions() != dimensions_) {
        throw std::runtime_error("Vector dimensions do not fit !");
    }
    Eigen::Ref<Eigen::Vector3f> fixed_size_other((Eigen::VectorXf &) other_ptr->getData());
    push_on_ptr->setData(Eigen::Ref<Eigen::Vector3f>(*vector_).cross(fixed_size_other));
}

void datatypes::Vector::mul(Matrix2D *other_ptr, Vector *push_on_ptr) {
    if (other_ptr->getSizeY() != dimensions_) {
        throw std::runtime_error("Vector, Matrix dimensions do not fit !");
    }
    push_on_ptr->setData(other_ptr->getData() * *vector_);
    push_on_ptr->setDimensions(dimensions_);
}

auto datatypes::Vector::getData() -> Eigen::VectorXf & {
    return *vector_;
}

void datatypes::Vector::setData(const Eigen::VectorXf &new_vector) {
    *vector_ = new_vector;
}

void datatypes::Vector::setDimensions(unsigned int new_size) {
    dimensions_ = new_size;
}

auto datatypes::Vector::getDimensions() -> unsigned int {
    return dimensions_;
}

void datatypes::Vector::equals(Vector *other_ptr, Boolean *push_on_ptr) {
    push_on_ptr->setData(*vector_ == other_ptr->getData());
}

void datatypes::Vector::print() {
    std::cout << *vector_ << std::endl;
}
