//
// Created by revol-xut on 24.01.20.
//

#include "../../include/data_classes/matrix.hpp"

#include <iostream>

datatypes::Matrix2D::Matrix2D(unsigned cols, unsigned rows) {
    this->setType(kMatrix2D);
    cols_ = cols;
    rows_ = rows;
    eigen_matrix_ = std::make_shared<Eigen::MatrixXf>(rows, cols); //Rows / Colls
}

datatypes::Matrix2D::~Matrix2D() = default;

auto datatypes::Matrix2D::expected_size() -> unsigned {
    return cols_ * rows_ * sizeof(defaultType);
}

void datatypes::Matrix2D::parse(char *data_ptr) {
    /*
     * Chunk of Data will be grabbed converted into float value and placed from left to right.
     * The Data is expected to be already checked for validity e.g size etc.
    */
    unsigned int remainder;
    int size_x = static_cast<int>(cols_);
    for (int i = 0; i < cols_ * rows_; i++) {
        // This can possible throw a segmentation fault
        remainder = static_cast<unsigned int>(std::div(i, size_x).quot);
        (*eigen_matrix_)(remainder, i % cols_) = datatypes::castToValue(data_ptr + i * sizeof(float));;
    }
}

void datatypes::Matrix2D::serialize(char *data_ptr) {
    for (int i = 0; i < cols_; i++) {
        for (int j = 0; j < rows_; j++) {
            datatypes::degrade((*eigen_matrix_)(i, j), data_ptr + (i * cols_ + j) * sizeof(float));
        }
    }
}

auto datatypes::Matrix2D::getData() -> const Eigen::MatrixXf & {
    return *this->eigen_matrix_;
}

auto datatypes::Matrix2D::getSizeX() const -> unsigned int {
    return cols_;
}

auto datatypes::Matrix2D::getSizeY() const -> unsigned int {
    return rows_;
}

void datatypes::Matrix2D::setSize(unsigned int size_x, unsigned int size_y) {
    cols_ = size_x;
    rows_ = size_y;
}

void datatypes::Matrix2D::setData(const Eigen::MatrixXf &eigen_matrix) {
    *eigen_matrix_ = eigen_matrix;
}

void
datatypes::Matrix2D::add(Matrix2D *other_ptr, Matrix2D *push_on_ptr) {
    if (other_ptr->getSizeX() != cols_ || other_ptr->getSizeY() != rows_) {
        throw std::runtime_error("Dimensions of matrices to not fit");
    }
    push_on_ptr->setData(other_ptr->getData() + *eigen_matrix_);
    push_on_ptr->setSize(cols_, rows_);
}

void datatypes::Matrix2D::add(Scalar *other_ptr, Matrix2D *push_on_ptr) {
    // Generates a matrix filled with the value of the scalar value and adds it
    push_on_ptr->setData(*eigen_matrix_ + Eigen::Matrix2f::Constant(other_ptr->getData()));
    push_on_ptr->setSize(cols_, rows_);
}

void
datatypes::Matrix2D::mul(Matrix2D *other_ptr, Matrix2D *push_on_ptr) {
    if (other_ptr->getSizeY() != cols_ && other_ptr->getSizeX() != rows_) {
        throw std::runtime_error("Dimensions of matrices to not fit");
    }
    push_on_ptr->setData(other_ptr->getData() + *eigen_matrix_);
}

void datatypes::Matrix2D::mul(Scalar *other_ptr, Matrix2D *push_on_ptr) {
    push_on_ptr->setData(*eigen_matrix_ * static_cast<float>(other_ptr->getData()));
    push_on_ptr->setSize(cols_, rows_);
}

void datatypes::Matrix2D::mul(Vector *other_ptr, Vector *push_on_ptr) {
    push_on_ptr->setData(*eigen_matrix_ * other_ptr->getData());
    push_on_ptr->setDimensions(static_cast<unsigned int>(other_ptr->getDimensions()));
}

void datatypes::Matrix2D::equals(Matrix2D *other_ptr, Boolean *push_on_ptr) {
    push_on_ptr->setData(*eigen_matrix_ == other_ptr->getData());
}