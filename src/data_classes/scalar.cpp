//
// Created by revol-xut on 25.01.20.
//

#include <iostream>

#include "../../include/data_classes/scalar.hpp"
#include "../../include/data_classes/vector.hpp"

datatypes::Scalar::Scalar(float value) {
    value_ = value;
}

void datatypes::Scalar::parse(char *data_ptr) {
    value_ = datatypes::castToValue(data_ptr);
}

void datatypes::Scalar::serialize(char *data_ptr) {
    datatypes::degrade(value_, data_ptr);
}

unsigned int datatypes::Scalar::expected_size() {
    return sizeof(defaultType);
}

auto datatypes::Scalar::getData() -> defaultType {
    return value_;
}

void datatypes::Scalar::setData(defaultType value) {
    value_ = value;
}

void datatypes::Scalar::add(Scalar *other_ptr, Scalar *push_on_ptr) {
    push_on_ptr->setData(value_ + other_ptr->getData());
}

void datatypes::Scalar::add(Matrix2D *other_ptr, Matrix2D *push_on_ptr) {
    push_on_ptr->setData(
            other_ptr->getData() + Eigen::Matrix2Xf::Constant(other_ptr->getSizeX(), other_ptr->getSizeY(), value_));
    push_on_ptr->setSize(other_ptr->getSizeX(), other_ptr->getSizeY());
}

void datatypes::Scalar::add(Vector *other_ptr, Vector *push_on_ptr) {
    push_on_ptr->setData(other_ptr->getData() + Eigen::VectorXf::Constant(other_ptr->getDimensions(), value_));
    push_on_ptr->setDimensions(other_ptr->getDimensions());
}

void datatypes::Scalar::mul(Matrix2D *other_ptr, Matrix2D *push_on_ptr) {
    push_on_ptr->setData(other_ptr->getData() * value_);
    push_on_ptr->setSize(other_ptr->getSizeX(), other_ptr->getSizeY());
}

void datatypes::Scalar::mul(Scalar *other_ptr, Scalar *push_on_ptr) {
    push_on_ptr->setData(value_ * other_ptr->getData());
}

void datatypes::Scalar::mul(Vector *other_ptr, Vector *push_on_ptr) {
    push_on_ptr->setData(other_ptr->getData() * value_);
    push_on_ptr->setDimensions(other_ptr->getDimensions());
}

void datatypes::Scalar::equals(Scalar *other_ptr, Boolean *push_on_ptr) {
    push_on_ptr->setData(value_ == other_ptr->getData());
}

void datatypes::Scalar::print() {
    std::cout << value_ << std::endl;
}