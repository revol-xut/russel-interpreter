//
// Created by revol-xut on 24.01.20.
//

#include "../../include/data_classes/data_class_interface.hpp"

auto datatypes::castToValue(const char *data) -> datatypes::defaultType {
    datatypes::defaultType return_value;
    *((char *) (&return_value)) = data[0];
    *((char *) (&return_value) + 1) = data[1];
    *((char *) (&return_value) + 2) = data[2];
    *((char *) (&return_value) + 3) = data[3];

    return return_value;
}

void datatypes::degrade(defaultType value, char *return_value) {
    return_value[0] = *((char *) (&value) + 0);
    return_value[1] = *((char *) (&value) + 1);
    return_value[2] = *((char *) (&value) + 2);
    return_value[3] = *((char *) (&value) + 3);
}