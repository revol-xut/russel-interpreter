//
// Created by revol-xut on 28.01.20.
//

#include "../../include/data_classes/abstract_type.hpp"

datatypes::AbstractType::AbstractType(unsigned int type, void *ptr) {
    type_ = type;
    ptr_ = ptr;
}

auto datatypes::AbstractType::getScalar() -> datatypes::Scalar * {
    return (datatypes::Scalar *) ptr_;
}

auto datatypes::AbstractType::getVector() -> datatypes::Vector * {
    return (datatypes::Vector *) ptr_;
}

auto datatypes::AbstractType::getMatrix2D() -> datatypes::Matrix2D * {
    return (datatypes::Matrix2D *) ptr_;
}

datatypes::Boolean *datatypes::AbstractType::getBoolean() {
    return (datatypes::Boolean *) ptr_;
}

auto datatypes::AbstractType::getType() const -> unsigned int {
    return type_;
}

auto datatypes::AbstractType::getSize() -> unsigned int {
    return ((datatypes::DataClassInterface *) ptr_)->expected_size();
}

void datatypes::AbstractType::serializeObject(char *data) {
    ((datatypes::DataClassInterface *) ptr_)->serialize(data);
}