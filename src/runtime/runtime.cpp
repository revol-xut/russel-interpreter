//
// Created by revol-xut on 28.01.20.
//

#include <iostream>

#include "../../include/runtime/runtime.hpp"

runtime::Runtime::Runtime(const std::shared_ptr<char[]> &data, const std::shared_ptr<char[]> &program,
                          unsigned int size_routine, unsigned int data_size) {
    variable_count_ = 0;
    variables_ = {};
    routine_ = std::make_shared<runtime::Routine>(program, size_routine);
    data_ = data;
    errors_ = {};
    data_field_size_ = data_size;
}

runtime::Runtime::Runtime(const std::shared_ptr<Routine> &routine) {
    routine_ = routine;
    variable_count_ = 0;
    variables_ = {};
    data_ = nullptr;
    errors_ = {};
}

runtime::Runtime::~Runtime() {
    for (datatypes::AbstractType var : variables_) {

        switch (var.getType()) {
            case datatypes::kNumeric:
                delete var.getScalar();
                break;
            case datatypes::kVector:
                delete var.getVector();
                break;
            case datatypes::kMatrix2D:
                delete var.getMatrix2D();
                break;
            case datatypes::kBoolean:
                delete var.getBoolean();
                break;
        }
    }
    variables_.clear();
    errors_.clear();
}

void runtime::Runtime::setData(const std::shared_ptr<char[]> &data, unsigned int data_size) {
    data_ = data;
    data_field_size_ = data_size;
}

auto runtime::Runtime::getVariable(unsigned int index) -> datatypes::AbstractType & {
    if (index < variables_.size()) {
        return variables_.at(index);
    }
    throw std::exception();
}

void runtime::Runtime::operator()() {
    execute();
}

void runtime::Runtime::execute() {
    char *temp_data;
    auto command = std::make_shared<runtime::Command>();
    datatypes::Vector *vector_ptr;
    datatypes::Matrix2D *matrix_ptr;
    datatypes::Scalar *scalar_ptr;
    datatypes::Boolean *boolean_ptr;
    unsigned int index_1, index_2, push_on, calculated_size;

    while (routine_->getDone()) {

        routine_->next(command);

        switch (command->command) {

            case 0x1:
                index_1 = static_cast<unsigned int>(command->arg1);
                if (variables_[index_1].getType() == datatypes::kNumeric) {
                    variables_[index_1].getScalar()->print();
                } else if (variables_[index_1].getType() == datatypes::kVector) {
                    variables_[index_1].getVector()->print();
                } else {
                    std::cout << variables_[index_1].getMatrix2D()->getData() << std::endl;
                }
                command->jump = 2;
                break;

                // Declares Byte
            case 0xA:
                break;

                // Declares Int - Not implemented
            case 0xB:
                break;

                // Declares Float Scalar Value
            case 0xC:

                calculated_size = byte_read_ + sizeof(float);
                if (calculated_size > data_field_size_ and calculated_size != data_field_size_) {
                    generate_not_enough_mem_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                scalar_ptr = new datatypes::Scalar(0);
                variables_.emplace_back(datatypes::kNumeric, scalar_ptr);
                temp_data = this->data_.get() + byte_read_;
                scalar_ptr->parse(temp_data);
                byte_read_ += scalar_ptr->expected_size();
                variable_count_++;
                command->jump = 1;
                break;

                // Declares Vector with size see arg1
            case 0xD:

                index_1 = static_cast<unsigned int>(command->arg1);

                if (index_1 >= variable_count_) {
                    generate_outof_bounds_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                if (get_datatype(index_1) != datatypes::kNumeric) {
                    generate_invalid_typ_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                index_2 = get_size_from_var(index_1);
                calculated_size = byte_read_ + sizeof(float) * index_2;

                if (calculated_size > data_field_size_ and calculated_size != data_field_size_) {
                    generate_not_enough_mem_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                vector_ptr = new datatypes::Vector(index_2);
                variables_.emplace_back(datatypes::kVector, vector_ptr);
                vector_ptr->parse(this->data_.get() + byte_read_);
                byte_read_ += vector_ptr->expected_size();
                variable_count_++;
                command->jump = 2;
                break;

                // Declares 2D Matrix with arg1 and arg2 which are the x and y sizes
            case 0xE:
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);

                if (index_1 >= variable_count_ or index_2 >= variable_count_) {
                    generate_outof_bounds_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                if (get_datatype(index_1) != datatypes::kNumeric or get_datatype(index_2) != datatypes::kNumeric) {
                    generate_invalid_typ_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                calculated_size = byte_read_ + sizeof(float) * get_size_from_var(index_2) * get_size_from_var(index_1);
                if (calculated_size > data_field_size_ and calculated_size != data_field_size_) {
                    std::cout << "size:" << calculated_size << std::endl;
                    generate_not_enough_mem_error(routine_->get_current_position(), *command);
                    exit_status_ = -10;
                    return;
                }

                matrix_ptr = new datatypes::Matrix2D(get_size_from_var(index_1), get_size_from_var(index_2));
                variables_.emplace_back(datatypes::kMatrix2D, matrix_ptr);
                matrix_ptr->parse(data_.get() + byte_read_);
                byte_read_ += matrix_ptr->expected_size();
                command->jump = 3;
                variable_count_++;
                break;

                // Declares Boolean value
            case 0xF:
                boolean_ptr = new datatypes::Boolean();
                boolean_ptr->parse(data_.get() + byte_read_);
                byte_read_ += boolean_ptr->expected_size();
                command->jump = 1;
                variable_count_++;
                break;

                // Adds to variables
            case 0x14:
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);
                push_on = static_cast<unsigned int>(command->arg3);
                this->add(index_1, index_2, push_on);
                command->jump = 4;
                break;

                // Multiplies to variables
            case 0x16:
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);
                push_on = static_cast<unsigned int>(command->arg3);
                this->mul(index_1, index_2, push_on);
                command->jump = 4;
                break;


            case 0x1E: // jmp
                index_1 = static_cast<unsigned int>(command->arg1);

                if (variables_[index_1].getType() == datatypes::kNumeric) {
                    routine_->setPointer(variables_[index_1].getScalar()->getData());
                    command->jump = 0;
                }
                command->jump = 2;
                break;

            case 0x1F: // if
                index_1 = static_cast<unsigned int>(command->arg1); // Pointer to boolean value
                push_on = static_cast<unsigned int>(command->arg2); // Jump address

                if (variables_[index_1].getType() == datatypes::kBoolean) {
                    if (variables_[index_1].getBoolean()->getValue()) {
                        routine_->setPointer(variables_[push_on].getScalar()->getData());
                        command->jump = 0;
                        break;
                    }
                }
                command->jump = 3;
                break;

            case 0x33: // Exit
                index_1 = static_cast<unsigned int>(command->arg1); // Pointer to exit status

                if (variables_[index_1].getType() == datatypes::kNumeric) {
                    exit_status_ = variables_[index_1].getScalar()->getData();
                    return;
                }

            case 0x50: // Equals
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);
                push_on = static_cast<unsigned int>(command->arg3);
                this->equals(index_1, index_2, push_on);
                command->jump = 4;

            case 0x51: // Negate
                index_1 = static_cast<unsigned int>(command->arg1);
                //this->equals(index_1, index_2, push_on);
                command->jump = 2;
            default:
                break;
        }
    }
    }

void runtime::Runtime::add(unsigned int index_1, unsigned int index_2, unsigned int push_on) {

    unsigned int type1, type2, pushon_type;

    type1 = variables_[index_1].getType();
    type2 = variables_[index_2].getType();
    pushon_type = variables_[push_on].getType();


    if (type1 == datatypes::kNumeric && type2 == datatypes::kNumeric && pushon_type == datatypes::kNumeric) {
        variables_[index_1].getScalar()->add(variables_[index_2].getScalar(), variables_[push_on].getScalar());
    } else if (type1 == datatypes::kNumeric && type2 == datatypes::kVector && pushon_type == datatypes::kVector) {
        variables_[index_1].getScalar()->add(variables_[index_2].getVector(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kNumeric && type2 == datatypes::kMatrix2D && pushon_type == datatypes::kMatrix2D) {
        variables_[index_1].getScalar()->add(variables_[index_2].getMatrix2D(), variables_[push_on].getMatrix2D());
    } else if (type1 == datatypes::kVector && type2 == datatypes::kNumeric && pushon_type == datatypes::kVector) {
        variables_[index_1].getVector()->add(variables_[index_2].getScalar(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kVector && type2 == datatypes::kVector && pushon_type == datatypes::kVector) {
        variables_[index_1].getVector()->add(variables_[index_2].getVector(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kMatrix2D && type2 == datatypes::kNumeric && pushon_type == datatypes::kMatrix2D) {
        variables_[index_1].getMatrix2D()->add(variables_[index_2].getScalar(), variables_[push_on].getMatrix2D());
    } else if (type1 == datatypes::kMatrix2D && type2 == datatypes::kMatrix2D && pushon_type == datatypes::kMatrix2D) {
        variables_[index_1].getMatrix2D()->add(variables_[index_2].getMatrix2D(), variables_[push_on].getMatrix2D());
    } else {
        // throw exception unknown add operation
    }
}

void runtime::Runtime::mul(unsigned int index_1, unsigned int index_2, unsigned int push_on) {
    unsigned int type1, type2, pushon_type;

    type1 = variables_[index_1].getType();
    type2 = variables_[index_2].getType();
    pushon_type = variables_[push_on].getType();

    if (type1 == datatypes::kNumeric && type2 == datatypes::kNumeric && pushon_type == datatypes::kNumeric) {
        variables_[index_1].getScalar()->mul(variables_[index_2].getScalar(), variables_[push_on].getScalar());
    } else if (type1 == datatypes::kNumeric && type2 == datatypes::kVector && pushon_type == datatypes::kVector) {
        variables_[index_1].getScalar()->mul(variables_[index_2].getVector(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kNumeric && type2 == datatypes::kMatrix2D && pushon_type == datatypes::kMatrix2D) {
        variables_[index_1].getScalar()->mul(variables_[index_2].getMatrix2D(), variables_[push_on].getMatrix2D());
    } else if (type1 == datatypes::kVector && type2 == datatypes::kNumeric && pushon_type == datatypes::kVector) {
        variables_[index_1].getVector()->mul(variables_[index_2].getScalar(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kVector && type2 == datatypes::kVector && pushon_type == datatypes::kVector) {
        variables_[index_1].getVector()->cross_mul(variables_[index_2].getVector(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kVector && type2 == datatypes::kVector && pushon_type == datatypes::kNumeric) {
        variables_[index_1].getVector()->dot_mul(variables_[index_2].getVector(), variables_[push_on].getScalar());
    } else if (type1 == datatypes::kMatrix2D && type2 == datatypes::kNumeric && pushon_type == datatypes::kMatrix2D) {
        variables_[index_1].getMatrix2D()->mul(variables_[index_2].getScalar(), variables_[push_on].getMatrix2D());
    } else if (type1 == datatypes::kMatrix2D && type2 == datatypes::kVector && pushon_type == datatypes::kVector) {
        variables_[index_1].getMatrix2D()->mul(variables_[index_2].getVector(), variables_[push_on].getVector());
    } else if (type1 == datatypes::kMatrix2D && type2 == datatypes::kMatrix2D && pushon_type == datatypes::kMatrix2D) {
        variables_[index_1].getMatrix2D()->mul(variables_[index_2].getMatrix2D(), variables_[push_on].getMatrix2D());
    } else {
        // throw exception unknown add operation
    }
}

void runtime::Runtime::equals(unsigned int index_1, unsigned int index_2, unsigned int push_on) {
    unsigned int type1, type2, pushon_type;

    type1 = variables_[index_1].getType();
    type2 = variables_[index_2].getType();
    pushon_type = variables_[push_on].getType();

    if (type1 != type2) { // Already wrong datatype no further check needed
        variables_[push_on].getBoolean()->setData(false);
        return;
    }

    if (type1 == datatypes::kNumeric and type2 == datatypes::kNumeric and pushon_type == datatypes::kBoolean) {
        variables_[index_1].getScalar()->equals(variables_[index_2].getScalar(), variables_[push_on].getBoolean());
    } else if (type1 == datatypes::kVector and type2 == datatypes::kVector and pushon_type == datatypes::kBoolean) {
        variables_[index_1].getVector()->equals(variables_[index_2].getVector(), variables_[push_on].getBoolean());
    } else if (type1 == datatypes::kMatrix2D and type2 == datatypes::kMatrix2D and pushon_type == datatypes::kBoolean) {
        variables_[index_1].getMatrix2D()->equals(variables_[index_2].getMatrix2D(),
                                                  variables_[push_on].getBoolean());
    } else if (type1 == datatypes::kBoolean and type2 == datatypes::kBoolean and pushon_type == datatypes::kBoolean) {
        variables_[index_1].getBoolean()->equals(variables_[index_2].getBoolean(),
                                                 variables_[push_on].getBoolean());
    }
}

auto runtime::Runtime::getVariableCount() const -> unsigned int {
    return variable_count_;
}

auto runtime::Runtime::get_size_from_var(unsigned int index) -> unsigned int {

    if (variable_count_ < index) {
        auto quick_error = std::make_shared<Error>();
        quick_error->message = "Invalid index";
        quick_error->error_code = 1;
        errors_.push_back(quick_error);
        throw std::runtime_error("Tries to access var that does not exists");
    }

    datatypes::AbstractType abstractType = variables_.at(index);

    if (abstractType.getType() != datatypes::kNumeric) {
        auto quick_error = std::make_shared<Error>();
        quick_error->message = "Index is not numeric !";
        quick_error->error_code = 1;
        errors_.push_back(quick_error);
        throw std::runtime_error("Invalid data type");
    }

    return static_cast<unsigned int>(abstractType.getScalar()->getData());
}

auto runtime::Runtime::get_exit_status() -> int {
    return (int) exit_status_;
}

auto runtime::Runtime::get_datatype(unsigned int index) -> unsigned int {
    return variables_[index].getType();
}

void runtime::Runtime::generate_invalid_typ_error(unsigned int position, runtime::Command command) {
    auto error = std::make_shared<Error>();
    error->message = "Invalid Datatype for Scalar Typ Construction";
    error->error_code = kErrorInvalidTyp;
    error->byte_code_position = position;
    error->failed_command = command;
    errors_.push_back(error);
}

void runtime::Runtime::generate_outof_bounds_error(unsigned int position, runtime::Command command) {
    auto error = std::make_shared<Error>();
    error->message = "No Variable with this index existis";
    error->error_code = kErrorOutOfBounds;
    error->byte_code_position = position;
    error->failed_command = command;
    errors_.push_back(error);
}

void runtime::Runtime::generate_not_enough_mem_error(unsigned int position, runtime::Command command) {
    auto error = std::make_shared<Error>();
    error->message =
            "Not enough data for all varialbes is provided provides amount: " + std::to_string(data_field_size_) +
            " current: " + std::to_string(byte_read_);
    error->error_code = kErrorMemoryOutOfBounds;
    error->byte_code_position = position;
    error->failed_command = command;
    errors_.push_back(error);

}

auto runtime::Runtime::get_errors() -> std::vector<std::shared_ptr<Error>> {
    return errors_;
}
