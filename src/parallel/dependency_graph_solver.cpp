//
// Created by einspaten on 06.03.20.
//

#include "../../include/parallel/dependency_graph_solver.hpp"


DependencyGraphSolver::DependencyGraphSolver(
        const std::shared_ptr<GraphConstructor> &new_ptr) {
    tree_ = new_ptr;
}


DependencyGraphSolver::~DependencyGraphSolver() = default;

void DependencyGraphSolver::solve() {
    std::vector<std::shared_ptr<DependencyVertex>> current = tree_->get_first_layer_vertices();
    std::vector<std::shared_ptr<DependencyVertex>> temporary = {};
    std::vector<std::shared_ptr<DependencyVertex>> second_temporary = {};
    unsigned int step = 0;

    for (const std::shared_ptr<DependencyVertex> &current_ptr : current) {
        current_ptr->set_step(step);
        current_ptr->set_satisfied();
    }

    while (not current.empty()) {
        step++;

        for (const std::shared_ptr<DependencyVertex> &current_ptr : current) {
            current_ptr->resolve_next_wave_vertices(temporary);
        }
        for (const std::shared_ptr<DependencyVertex> &current_ptr : temporary) {
            current_ptr->set_step(step);
            current_ptr->set_satisfied();
        }

        current = temporary;
        temporary.clear();
        second_temporary.clear();

    }

    highest_step_counter = step;
}


auto DependencyGraphSolver::highest_step_count() -> unsigned int {
    return highest_step_counter;
}
