//
// Created by einspaten on 22.03.20.
//

#include "../../include/parallel/dependency_vertex.hpp"

DependencyVertex::DependencyVertex(MemoryManagement *mem_m) {
    mem_m_ = mem_m;
    step_ = std::numeric_limits<unsigned int>::max();
    satisfied_ = false;
    my_id_ = std::numeric_limits<unsigned int>::max();
}

DependencyVertex::DependencyVertex(MemoryManagement *mem_m, const runtime::Command &data) {
    mem_m_ = mem_m;
    data_ = data;
    step_ = std::numeric_limits<unsigned int>::max();
    satisfied_ = false;
    my_id_ = std::numeric_limits<unsigned int>::max();
}

DependencyVertex::~DependencyVertex() = default;

void DependencyVertex::connect_to_vertex(unsigned int other_vertex_id) {
    for (unsigned int id : depending_on_) {
        if (id == other_vertex_id) {
            return;
        }
    }

    depending_on_.push_back(other_vertex_id);
    mem_m_->resolve_vertex_id(other_vertex_id)->vertex_call_back(my_id_);
}

void DependencyVertex::vertex_call_back(unsigned int other_vertex_id) {
    for (unsigned int id : is_important_for_) {
        if (id == other_vertex_id) {
            return;
        }
    }
    is_important_for_.push_back(other_vertex_id);
}

auto DependencyVertex::all_dependencies_satisfied() -> bool {
    for (unsigned int id : depending_on_) {
        if (not mem_m_->resolve_vertex_id(id)->is_satisfied()) {
            return false;
        }
    }
    return true;
}

auto DependencyVertex::get_data() -> runtime::Command {
    return data_;
}

void DependencyVertex::set_data(const runtime::Command &data) {
    data_ = data;
}

void DependencyVertex::set_satisfied() {
    satisfied_ = true;
}

auto DependencyVertex::is_satisfied() -> bool {
    return satisfied_;
}

void DependencyVertex::set_step(unsigned int step) {
    step_ = step;
}

auto DependencyVertex::get_step() -> unsigned int {
    return step_;
}

void DependencyVertex::set_id(unsigned int id) {
    my_id_ = id;
}

auto DependencyVertex::get_id() -> unsigned int {
    return my_id_;
}

void DependencyVertex::resolve_next_wave_vertices(std::vector<std::shared_ptr<DependencyVertex> > &other_list) {
    std::shared_ptr<DependencyVertex> current_ptr;

    for (unsigned int i : is_important_for_) {
        current_ptr = mem_m_->resolve_vertex_id(i);

        if (current_ptr != nullptr and current_ptr->all_dependencies_satisfied() and not current_ptr->satisfied_) {
            bool contains = false;

            for (const std::shared_ptr<DependencyVertex> &temp : other_list) { // Removes unnecessary duplicates
                if (current_ptr == temp) {
                    contains = true;
                    break;
                }
            }
            if (not contains) {
                other_list.push_back(current_ptr);
            }
        }
    }
}