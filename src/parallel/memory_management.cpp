//
// Created by einspaten on 22.03.20.
//

#include "../../include/parallel/memory_management.hpp"

#include <algorithm>

MemoryManagement::MemoryManagement() {
    vertices_ = {};
}

MemoryManagement::~MemoryManagement() = default;

auto MemoryManagement::get_id(const std::shared_ptr<DependencyVertex> &other_vertex) -> unsigned int {
    unsigned int counter = 0;

    for (const std::shared_ptr<DependencyVertex> &vertex : vertices_) {
        if (other_vertex == vertex) {
            return counter;
        }
        counter++;
    }

    throw std::runtime_error("MemoryManager does not contain such object !");
}

auto MemoryManagement::resolve_vertex_id(unsigned int id) -> std::shared_ptr<DependencyVertex> & {
    return vertices_[id];
}

auto MemoryManagement::add_vertex(const runtime::Command data) -> unsigned int {
    auto new_object = std::make_shared<DependencyVertex>(this, data);
    vertices_.push_back(new_object);

    unsigned int id = vertices_.size() - 1;

    new_object->set_id(id);
    return id;
}

auto MemoryManagement::create_empty_vertex() -> unsigned int {
    auto new_object = std::make_shared<DependencyVertex>(this);
    vertices_.push_back(new_object);

    unsigned int id = vertices_.size() - 1;

    new_object->set_id(id);
    return id;
}

auto MemoryManagement::get_size() -> unsigned int {
    return vertices_.size();
}

auto
MemoryManagement::cut_part(unsigned int start, unsigned int end) -> std::vector<std::shared_ptr<DependencyVertex> > {
    end = std::min(static_cast<unsigned int>(vertices_.size()), end);

    auto first = vertices_.begin() + start;
    auto last = vertices_.begin() + end;

    return std::vector<std::shared_ptr<DependencyVertex>>(first, last);
}

auto MemoryManagement::return_vertices_with_step(unsigned int step) -> std::vector<std::shared_ptr<DependencyVertex> > {
    std::vector<std::shared_ptr<DependencyVertex> > return_value = {};

    for (const std::shared_ptr<DependencyVertex> &vertex : vertices_) {
        if (vertex->get_step() == step) {
            return_value.push_back(vertex);
        }
    }
    return return_value;
}