//
// Created by einspaten on 07.03.20.
//

#include "../../include/parallel/code2treeparser.hpp"


Code2TreeParser::Code2TreeParser(const std::shared_ptr<char[]> &program, unsigned int size) {
    routine_ = std::make_shared<runtime::Routine>(program, size);
    tree_ = std::make_shared<GraphConstructor>();
}

Code2TreeParser::Code2TreeParser(const std::shared_ptr<runtime::Routine> &program) {
    routine_ = program;
    tree_ = std::make_shared<GraphConstructor>();
}

Code2TreeParser::~Code2TreeParser() = default;


void Code2TreeParser::parse() {
    char *temp_data;
    auto command = std::make_shared<runtime::Command>();
    unsigned int index_1, index_2, push_on;
    unsigned int current_line = 0;
    unsigned int variable_index_1, variable_index_2, variable_index_push_on, temporary;
    unsigned int variable_counter = 0;
    while (routine_->getDone()) {

        routine_->next(command);

        switch (command->command) {

            case 0x28: // Print
                index_1 = static_cast<unsigned int>(command->arg1);
                variable_index_1 = tree_->generate_passive_vertex(*command);
                tree_->connect_vertex(variable_index_1, tree_->get_index_of_latest_var(index_1));
                command->jump = 2;
                break;

                // Declares Byte
            case 0xA:
                break;

                // Declares Int - Not implemented
            case 0xB:
                break;

                // Declares Float Scalar Value
            case 0xC:
                tree_->add_vertex(variable_counter, *command);
                command->jump = 1;
                variable_counter++;
                break;

                // Declares Vector with size see arg1
            case 0xD:

                index_1 = static_cast<unsigned int>(command->arg1);
                variable_index_1 = tree_->get_index_of_latest_var(index_1);
                variable_index_2 = tree_->add_vertex(variable_counter, *command);
                tree_->connect_vertex(variable_index_1, variable_index_2);
                command->jump = 2;
                variable_counter++;
                break;

                // Declares 2D Matrix with arg1 and arg2 which are the x and y sizes
            case 0xE:
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);

                variable_index_2 = tree_->add_vertex(variable_counter, *command);

                variable_index_1 = tree_->get_index_of_latest_var(index_1);
                tree_->connect_vertex(variable_index_1, variable_index_2);

                variable_index_1 = tree_->get_index_of_latest_var(index_2);
                tree_->connect_vertex(variable_index_1, variable_index_2);

                command->jump = 3;
                variable_counter++;
                break;

                // Declares Boolean value
            case 0xF:
                tree_->add_vertex(variable_counter, *command);
                command->jump = 1;
                variable_counter++;
                break;

                // Adds to variables
            case 0x14:
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);
                push_on = static_cast<unsigned int>(command->arg3);

                variable_index_1 = tree_->get_index_of_latest_var(index_1);
                variable_index_2 = tree_->get_index_of_latest_var(index_2);

                temporary = tree_->add_vertex(push_on, *command); //TODO Change away from 0

                tree_->connect_vertex(temporary, variable_index_1);
                tree_->connect_vertex(temporary, variable_index_2);
                command->jump = 4;
                break;

                // Multiplies to variables
            case 0x16:
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);
                push_on = static_cast<unsigned int>(command->arg3);

                variable_index_1 = tree_->get_index_of_latest_var(index_1);
                variable_index_2 = tree_->get_index_of_latest_var(index_2);

                temporary = tree_->add_vertex(push_on, *command); //TODO Change away from 0

                tree_->connect_vertex(temporary, variable_index_1);
                tree_->connect_vertex(temporary, variable_index_2);
                command->jump = 4;
                break;
                // power of

            case 0x1E: // jmp
                index_1 = static_cast<unsigned int>(command->arg1);
                command->jump = 2;

            case 0x1F: // if
                index_1 = static_cast<unsigned int>(command->arg1); // Pointer to boolean value
                push_on = static_cast<unsigned int>(command->arg3); // Jump address

                //     tree_->new_control_vertex();
                //    tree_->get_control_vertex()->connect_to_vertex(tree_->get_vertex(variable_index_1));

                command->jump = 3;
                break;

            case 0x33: // Exit
                index_1 = static_cast<unsigned int>(command->arg1); // Pointer to exit status

                return;

            case 0x50: // Equals
                index_1 = static_cast<unsigned int>(command->arg1);
                index_2 = static_cast<unsigned int>(command->arg2);
                push_on = static_cast<unsigned int>(command->arg3);

                variable_index_1 = tree_->get_index_of_latest_var(index_1);
                variable_index_2 = tree_->get_index_of_latest_var(index_2);
                temporary = tree_->add_vertex(push_on, *command);

                tree_->connect_vertex(temporary, variable_index_1);
                tree_->connect_vertex(temporary, variable_index_2);
                command->jump = 4;

            case 0x51: // Negate
                index_1 = static_cast<unsigned int>(command->arg1);
                variable_index_1 = tree_->get_index_of_latest_var(index_1);
                temporary = tree_->add_vertex(index_1, *command);
                tree_->connect_vertex(temporary, variable_index_1);
            default:
                break;
        }
        current_line++;
    }
}


auto Code2TreeParser::getTree() -> std::shared_ptr<GraphConstructor> {
    return tree_;
}