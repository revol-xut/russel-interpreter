//
// Created by einspaten on 13.03.20.
//

#include "../../include/parallel/codesegment.hpp"

//#include "tree_constructor.hpp"
//#include "tree_constructor.cpp"




CodeSegment::CodeSegment(const std::shared_ptr<char[]> &program, unsigned int size) {
//    program_ = program;
    //   char *temp_program = new char[size];
    program_ = std::make_shared<char[]>(size);
    std::memcpy(program_.get(), program.get(), size);
    size_ = size;
    resulting_branches_ = false;
    comparison_ptr = program.get();
    ////  delete[] temp_program;
}

CodeSegment::CodeSegment(char *program, unsigned int size) {
    // program_ = std::shared_ptr<char[]>(static_cast<char*>(program) );

    //char *temp_program = new char[size];

    program_ = std::shared_ptr<char[]>(new char[size]);
    std::memcpy(program_.get(), program, size);
    size_ = size;
    resulting_branches_ = false;
    comparison_ptr = program;
}

CodeSegment::~CodeSegment() = default;


void CodeSegment::process() {

    auto code_parser = std::make_unique<Code2TreeParser>(program_, size_);
    code_parser->parse();

    auto tree = code_parser->getTree();
    required_vars_ = tree->get_required_vars();

    auto solver = std::make_unique<DependencyGraphSolver>(tree);
    solver->solve();

    for (unsigned int step_count = 0; step_count < solver->highest_step_count(); step_count++) {
        processed_graph_[step_count] = {};

        std::vector<std::shared_ptr<DependencyVertex>> temp = tree->get_vertices_with_step(step_count);
        for (const std::shared_ptr<DependencyVertex> &dep : temp) { //TODO: Maybe do that already internal in the tree constructor
            processed_graph_.at(step_count).push_back(dep->get_data());
        }
    }

}

void CodeSegment::set_resulting_branches(unsigned int boolean_index) {
    resulting_branches_ = true;
    index_bool_ = boolean_index;
}

void CodeSegment::default_branch(const std::shared_ptr<CodeSegment> &other_code_segment) {
    default_branch_ = other_code_segment;
}

void CodeSegment::other_branch(const std::shared_ptr<CodeSegment> &other_code_segment) {
    other_branch_ = other_code_segment;
}

auto CodeSegment::get_program() -> char * {
    return comparison_ptr;
}

auto CodeSegment::get_size() -> unsigned int {
    return size_;
}

auto CodeSegment::get_default_branch() -> std::shared_ptr<CodeSegment> {
    return default_branch_;
}

auto CodeSegment::get_other_branch() -> std::shared_ptr<CodeSegment> {
    return other_branch_;
}

auto CodeSegment::serialize() -> nlohmann::json {

    nlohmann::json data;
    std::string current_key;

    data["id"] = my_id_;
    data["branches_of"] = resulting_branches_;
    data["boolean_index"] = index_bool_;


    if (default_branch_ != nullptr) { // The last code segment
        data["default_branch"] = default_branch_->get_id();
    }

    if (other_branch_ != nullptr) {
        data["other_branch"] = other_branch_->get_id();
        required_vars_.push_back(index_bool_);
    }
    data["content"] = {};

    std::map<unsigned int, std::vector<runtime::Command>>::iterator it;

    for (it = processed_graph_.begin(); it != processed_graph_.end(); it++) {
        current_key = std::to_string(it->first);
        data["content"][current_key] = {};

        for (const runtime::Command &temp : it->second) {
            data["content"][current_key].push_back({temp.command, temp.arg1, temp.arg2, temp.arg3});
        }
    }

    data["required_vars"] = required_vars_;

    return data;
}


void CodeSegment::set_my_id(unsigned int id) {
    my_id_ = id;
}

auto CodeSegment::get_id() -> unsigned int {
    return my_id_;
}