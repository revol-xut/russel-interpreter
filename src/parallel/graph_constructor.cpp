//
// Created by einspaten on 22.03.20.
//
#include <memory>
#include <iostream>

#include "../../include/parallel/graph_constructor.hpp"

unsigned int GraphConstructor::variable_count_ = 0;

GraphConstructor::GraphConstructor() {
    mem_m_ = std::make_shared<MemoryManagement>();
}

GraphConstructor::~GraphConstructor() = default;

auto GraphConstructor::add_vertex(unsigned int var, runtime::Command data) -> unsigned int {
    if (mem_m_ == nullptr) {
        throw std::runtime_error("Memory Management didn't got initialised");
    }
    if (var >= variable_count_) {
        variable_count_++;
        add_var(var);
    }

    auto new_index = mem_m_->add_vertex(data);

    local_variable_map_[var] = new_index;
    return new_index;
}

void GraphConstructor::connect_vertex(unsigned int index1, unsigned int index2) {
    auto size = mem_m_->get_size();

    if (index1 < size and index2 < size) {
        add_var(index1);
        add_var(index2);
        mem_m_->resolve_vertex_id(index1)->connect_to_vertex(index2);
    } else {
        throw std::runtime_error("index1 or index2 out of bound - no vertex with this index exists");
    }
}

auto GraphConstructor::get_first_layer_vertices() -> std::vector<std::shared_ptr<DependencyVertex> > {
    return mem_m_->cut_part(0, variable_count_);
}

auto GraphConstructor::get_vertices_with_step(unsigned int step) -> std::vector<std::shared_ptr<DependencyVertex> > {
    return mem_m_->return_vertices_with_step(step);
}

auto GraphConstructor::get_index_of_latest_var(unsigned int variable) -> unsigned int {

    if (variable > variable_count_) {
        throw std::runtime_error("Tries to use variable that does not even exists");
    } else {

        if (local_variable_map_.find(variable) == local_variable_map_.end()) {
            auto new_index = mem_m_->create_empty_vertex();
            local_variable_map_[variable] = new_index;
            add_var(variable);
        }

        return local_variable_map_[variable];
    }
}

auto GraphConstructor::generate_passive_vertex(runtime::Command data) -> unsigned int {
    auto new_index = mem_m_->add_vertex(data);
    return new_index;
}

auto GraphConstructor::get_vertex(unsigned int index) -> unsigned int {
    return local_variable_map_[index];
}

void GraphConstructor::add_var(unsigned int var) {
    for (unsigned int x : required_vars_) {
        if (x == var) {
            return;
        }
    }

    required_vars_.push_back(var);

}

std::vector<unsigned int> GraphConstructor::get_required_vars() {
    return required_vars_;
}