//
// Created by einspaten on 16.03.20.
//

#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>
#include "../../include/routine/routine.hpp"
//#include <iomanip> // setw

#include "../../include/parallel/code_segment_generator.hpp"

std::map<char, unsigned int> CodeSegmentGenerator::jump_sizes = {};


CodeSegmentGenerator::CodeSegmentGenerator(const std::shared_ptr<char[]> &program, unsigned int size) {
    program_ = program;
    size_ = size;
    routine_ = std::make_shared<runtime::Routine>(program, size);
    parsed_code_segments_ = {};
    parsed_code_segments_.reserve(1);

    if (jump_sizes.empty()) {

        jump_sizes[0xA] = 1;
        jump_sizes[0xB] = 1;
        jump_sizes[0xC] = 1; // Dec Float
        jump_sizes[0xD] = 2; // Dec Vec
        jump_sizes[0xE] = 3; // Dec Mat2D
        jump_sizes[0xF] = 1; // Dec Bool
        jump_sizes[0x14] = 4; // Add
        jump_sizes[0x15] = 4; // Min
        jump_sizes[0x16] = 4; // Mul
        jump_sizes[0x17] = 1; // Pot (not implemented)
        jump_sizes[0x1E] = 2; // jmp
        jump_sizes[0x28] = 2; // write stdout
        jump_sizes[0x50] = 4; // Equals
        jump_sizes[0x51] = 2; // Boolean Negate
        jump_sizes[0x1F] = 3; // if
        jump_sizes[0x33] = 1; // Terminate
        jump_sizes[0x32] = 2; // Del
    }

}

CodeSegmentGenerator::~CodeSegmentGenerator() = default;


auto CodeSegmentGenerator::parse(unsigned int start_position,
                                 const std::shared_ptr<CodeSegment> &last_code_segment) -> std::shared_ptr<CodeSegment> {

    bool exit = false;
    auto command = std::make_shared<runtime::Command>();
    unsigned int start_code_segment = start_position;
    unsigned int current_position = start_position;
    std::shared_ptr<CodeSegment> temporary_ptr;
    unsigned int object_position;

    routine_->setPointer(current_position);

    while (routine_->getDone() and current_position < size_ and not exit) {

        routine_->next(command);

        switch (command->command) {

            case 0x1E: // jmp

                temporary_ptr = nullptr;
                if (not check_existing(program_.get() + start_code_segment,
                                       current_position - start_code_segment)) {

                    if (current_position - start_code_segment > 0) {
                        temporary_ptr = std::make_shared<CodeSegment>(program_.get() + start_code_segment,
                                                                      current_position - start_code_segment);
                        parsed_code_segments_.push_back(temporary_ptr);
                        temporary_ptr->set_my_id(parsed_code_segments_.size() - 1);
                        if (last_code_segment == nullptr) {
                            start_code_segment_ = temporary_ptr;
                        } else {
                            last_code_segment->default_branch(
                                    parsed_code_segments_.at(parsed_code_segments_.size() - 1));
                        }
                    }

                } else {
                    return get_object_from_ptr(program_.get() + start_code_segment,
                                               current_position - start_code_segment);
                }
                return parse(command->arg1, temporary_ptr);


            case 0x1F: // if
                if (check_existing(program_.get() + start_code_segment,
                                   current_position - start_code_segment)) {
                    return nullptr;
                }

                temporary_ptr = std::make_shared<CodeSegment>(program_.get() + start_code_segment,
                                                              (current_position + jump_sizes.at(command->command)) -
                                                              start_code_segment);
                parsed_code_segments_.push_back(temporary_ptr);
                object_position = parsed_code_segments_.size() - 1;
                temporary_ptr->set_resulting_branches(command->arg1);
                temporary_ptr->set_my_id(object_position);

                if (last_code_segment == nullptr) {
                    start_code_segment_ = temporary_ptr;
                } else {
                    last_code_segment->default_branch(parsed_code_segments_.at(object_position));
                }

                // Generates other path
                temporary_ptr = parse(command->arg2, parsed_code_segments_.at(object_position));
                parsed_code_segments_.at(object_position)->other_branch(temporary_ptr);

                temporary_ptr = parse(current_position + 3, parsed_code_segments_.at(object_position));
                parsed_code_segments_.at(object_position)->default_branch(temporary_ptr);

                return temporary_ptr;

            case 0x33: // Exit
                exit = true;
                break;

            default:

                if (jump_sizes.find(command->command) == jump_sizes.end()) {
                    throw std::runtime_error("There is no instruction behind this byte!");
                }

                command->jump = jump_sizes.at(command->command);
                current_position += jump_sizes.at(command->command);
                break;
        }
    }

    if (not check_existing(program_.get() + start_code_segment,
                           current_position - start_code_segment)) {

        unsigned int end_pos = std::min(current_position + jump_sizes.at(command->command), size_);
        auto temp = std::make_shared<CodeSegment>(program_.get() + start_code_segment, end_pos - start_code_segment);
        parsed_code_segments_.push_back(temp);
        temp->set_my_id(parsed_code_segments_.size() - 1);
        if (last_code_segment == nullptr) {
            start_code_segment_ = temp;
        } else {
            last_code_segment->default_branch(
                    parsed_code_segments_.at(parsed_code_segments_.size() - 1));
        }

        return parsed_code_segments_.at(parsed_code_segments_.size() - 1);
    } else {
        return get_object_from_ptr(program_.get() + start_code_segment,
                                   current_position - start_code_segment);
    }
}

void CodeSegmentGenerator::print_code_segments() {
    for (const std::shared_ptr<CodeSegment> &seg : parsed_code_segments_) {

    }
}

auto CodeSegmentGenerator::check_existing(const char *program, unsigned int size) -> bool {
    for (const std::shared_ptr<CodeSegment> &seg : parsed_code_segments_) {
        if (seg->get_program() == program and seg->get_size() == size) {
            return true;
        }
    }
    return false;
}

void CodeSegmentGenerator::further_segment_processing() {
    for (const std::shared_ptr<CodeSegment> &seg : parsed_code_segments_) {
        seg->process();
    }
}

auto CodeSegmentGenerator::get_object_from_ptr(const char *program, unsigned int size) -> std::shared_ptr<CodeSegment> {
    for (const std::shared_ptr<CodeSegment> &seg : parsed_code_segments_) {
        if (seg->get_program() == program and seg->get_size() == size) {
            return seg;
        }
    }
    return nullptr;
}

void CodeSegmentGenerator::save_to_file(const std::string &file_path) {

    nlohmann::json data;

    data["start_code_segment"] = start_code_segment_->get_id();

    std::string current_key;
    unsigned int count = 0;
    for (const std::shared_ptr<CodeSegment> &seg : parsed_code_segments_) {
        current_key = "segment_" + std::to_string(count);
        data[current_key] = seg->serialize();
        count++;
    }

    std::ofstream file;
    file.open(file_path); // Shpuld theoreticaly create file when not existent
    file << data << std::endl; // std::setw(4)

}