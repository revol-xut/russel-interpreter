//
// Created by revol-xut on 28.01.20.
//

#include "../../include/routine/routine.hpp"


runtime::Routine::Routine(const std::shared_ptr<char[]> &program, unsigned int size) {
    program_ = program;
    size_ = size;
    current_ = 0;
    done_ = false;
}

runtime::Routine::~Routine() = default;


auto runtime::Routine::getSize() const -> unsigned int {
    return size_;
}

auto runtime::Routine::getDone() const -> bool {
    return done_;
}

void runtime::Routine::setPointer(unsigned int new_current) {
    current_ = new_current;
}

void runtime::Routine::next(const std::shared_ptr<Command> &command) {

    current_ += command->jump;

    done_ = current_ < size_;

    command->command = program_[current_];
    command->arg1 = program_[current_ + 1];
    command->arg2 = program_[current_ + 2];
    command->arg3 = program_[current_ + 3];
    // Be aware pos. of segfault
}

auto runtime::Routine::getData() -> std::shared_ptr<char[]> {
    return program_;
}

void runtime::Routine::set_done_false() {
    done_ = false;
}

auto runtime::Routine::get_current_position() const -> unsigned int {
    return current_;
}