# Russel-Interpreter

Byte Code Interpreter for the Russel-Project

- currently very messy needs some revisions someday but need to define interface 

### Compilation

```shell script
 $ mkdir build && cd build 
 $ cmake .. && make
 $ make install
```

### Dependencies

 * [Eigen3](http://eigen.tuxfamily.org) Linear Algebra and Math Stuff
 * [Boost Test]( http://boost.org/libs/test) Only for unit tests

### Versions

* **v0.1b** (2020.5.29)

    * little changes to the interface
    * added boolean data type
    * added better error management
    * fixed major memory leak
    
* **v0.1a** (2020.2.17)
    
    * Added DataTypes (Matrix2D, Vector, Scalar)
    * Added Basic operators like add, mul or declare 
    * Added Routine class
    
