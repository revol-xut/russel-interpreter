//
// Created by revol-xut on 28.01.20.
//

#ifndef RUSSEL_INTERPRETER_ROUTINE_HPP
#define RUSSEL_INTERPRETER_ROUTINE_HPP

#include <array>
#include <memory>


namespace runtime {

    struct Command {
        // Command
        char command;
        // First and Second Argument
        char arg1;
        char arg2;
        char arg3;
        uint8_t jump = 0; // We technically only need 2 bits
    };

    class Routine {
    public:
        explicit Routine(const std::shared_ptr<char[]> &program, unsigned int size);

        ~Routine();

        auto getSize() const -> unsigned int;

        void next(const std::shared_ptr<Command> &command);

        auto getDone() const -> bool;

        auto get_current_position() const -> unsigned int;

        void setPointer(unsigned int new_current);

        auto getData() -> std::shared_ptr<char[]>;

        void set_done_false();

    private:

        bool done_ = true;
        unsigned int size_, current_ = 0;
        std::shared_ptr<char[]> program_;


    };
}


#endif //RUSSEL_INTERPRETER_ROUTINE_HPP
