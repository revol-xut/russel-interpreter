//
// Created by einspaten on 16.03.20.
//

#ifndef TESTPARSER_CODE_SEGMENT_GENERATOR_HPP
#define TESTPARSER_CODE_SEGMENT_GENERATOR_HPP

#include <vector>

#include "codesegment.hpp"

class CodeSegmentGenerator {
    /*!
     * @brief This Class generates entire code segments from russel byte code
     * A Code segment is part of the source code. Code segments are parsed by looking for program flow control statements like if or jmp.
     */
public:
    CodeSegmentGenerator(const std::shared_ptr<char[]> &program, unsigned int size);

    ~CodeSegmentGenerator();

    auto parse(unsigned int start_position,
               const std::shared_ptr<CodeSegment> &last_code_segment) -> std::shared_ptr<CodeSegment>;

    void further_segment_processing();

    void print_code_segments();

    void save_to_file(const std::string &file_path);

    auto get_object_from_ptr(const char *program, unsigned int size) -> std::shared_ptr<CodeSegment>;

    static std::map<char, unsigned int> jump_sizes;

private:
    auto check_existing(const char *program, unsigned int size) -> bool;

    std::shared_ptr<char[]> program_;
    unsigned int size_;
    std::vector<std::shared_ptr<CodeSegment>> parsed_code_segments_;
    std::shared_ptr<CodeSegment> start_code_segment_;

    std::shared_ptr<runtime::Routine> routine_;


};


#endif //TESTPARSER_CODE_SEGMENT_GENERATOR_HPP
