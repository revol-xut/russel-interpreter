//
// Created by einspaten on 22.03.20.
//

#ifndef PARSER_DEPENDENCY_VERTEX_HPP
#define PARSER_DEPENDENCY_VERTEX_HPP

#include "../routine/routine.hpp"
#include <memory>
#include <vector>

class MemoryManagement;

class DependencyVertex {
    /*!
     * @brief Vertex Class
     */
public:
    explicit DependencyVertex(MemoryManagement *mem_m);

    DependencyVertex(MemoryManagement *mem_m, const runtime::Command &data);

    ~DependencyVertex();

    void connect_to_vertex(unsigned int other_vertex_id);

    auto get_data() -> runtime::Command;

    void set_data(const runtime::Command &data);

    void set_satisfied();

    auto is_satisfied() -> bool;

    void set_step(unsigned int step);

    auto get_step() -> unsigned int;

    auto all_dependencies_satisfied() -> bool;

    /*!
     * @brief This method resolves all Vertices that are depending on the current Vertex and appends those that are full satisfied.
     */
    void resolve_next_wave_vertices(std::vector<std::shared_ptr<DependencyVertex>> &);

    auto get_id() -> unsigned int;

    void set_id(unsigned int id);

private:
    void vertex_call_back(unsigned int other_vertex_id);

    unsigned int step_;
    unsigned int my_id_;

    std::vector<unsigned int> depending_on_;
    std::vector<unsigned int> is_important_for_;

    runtime::Command data_;

    MemoryManagement *mem_m_;

    bool satisfied_;
};

#include "memory_management.hpp"

#endif //PARSER_DEPENDENCY_VERTEX_HPP
