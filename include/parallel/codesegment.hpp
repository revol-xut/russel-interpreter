//
// Created by einspaten on 13.03.20.
//

#ifndef TESTPARSER_CODESEGMENT_HPP
#define TESTPARSER_CODESEGMENT_HPP

#include <map>
#include <memory>
#include <vector>
#include <nlohmann/json.hpp>

#include "../routine/routine.hpp"

#include "code2treeparser.hpp"

#include "dependency_graph_solver.hpp"

class CodeSegment {
    /*!
     * @brief Data container for all Information relating given codesegment like source, graph, processed graph
     */
public:
    CodeSegment(const std::shared_ptr<char[]> &program, unsigned int size);

    CodeSegment(char *program, unsigned int size);

    ~CodeSegment();

    /*!
     * @brief Generates graph solves graph and formats the result
     */
    void process();

    /*!
     * @brief Toggles resulting branches flag to on with given boolean index
     */
    void set_resulting_branches(unsigned int boolean_index);

    void default_branch(const std::shared_ptr<CodeSegment> &other_code_segment);

    void other_branch(const std::shared_ptr<CodeSegment> &other_code_segment);

    auto get_program() -> char *;

    auto get_size() -> unsigned int;

    auto get_default_branch() -> std::shared_ptr<CodeSegment>;

    auto get_other_branch() -> std::shared_ptr<CodeSegment>;

    auto serialize() -> nlohmann::json;

    void set_my_id(unsigned int id);

    auto get_id() -> unsigned int;

private:
    // This Pointer is only used for comparison purposes to avoid having redundancies
    char *comparison_ptr = nullptr;

    std::shared_ptr<char[]> program_ = nullptr; // raw program
    std::map<unsigned int, std::vector<runtime::Command>> processed_graph_; // graph
    std::vector<unsigned int> required_vars_; // Which variables have to be evaluated before this code_segment can be executed

    bool resulting_branches_ = false; // Indicates that this code segment end with an if statement

    std::shared_ptr<CodeSegment> default_branch_ = nullptr; // Default following code segment
    std::shared_ptr<CodeSegment> other_branch_ = nullptr; // Used when if statement is false

    unsigned int index_bool_ = 0; // Uses for runtime evaluations
    unsigned int size_ = 0; // Size of char field which is the raw program

    unsigned int my_id_ = 0;


};


#endif //TESTPARSER_CODESEGMENT_HPP
