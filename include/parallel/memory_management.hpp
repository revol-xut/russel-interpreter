//
// Created by einspaten on 22.03.20.
//

#ifndef PARSER_MEMORY_MANAGEMENT_HPP
#define PARSER_MEMORY_MANAGEMENT_HPP

#include <vector>
#include <memory>

#include "dependency_vertex.hpp"

class MemoryManagement { // deallocating this object will deallocate all object in the Graph
public:
    MemoryManagement();

    ~MemoryManagement();

    auto resolve_vertex_id(unsigned int id) -> std::shared_ptr<DependencyVertex> &;

    auto add_vertex(runtime::Command ptr) -> unsigned int;

    auto get_id(const std::shared_ptr<DependencyVertex> &other_vertex) -> unsigned int;

    auto get_size() -> unsigned int;

    auto cut_part(unsigned int start, unsigned int end) -> std::vector<std::shared_ptr<DependencyVertex>>;

    auto return_vertices_with_step(unsigned int step) -> std::vector<std::shared_ptr<DependencyVertex>>;

    auto create_empty_vertex() -> unsigned int;

private:

    std::vector<std::shared_ptr<DependencyVertex>> vertices_;


};


#endif //PARSER_MEMORY_MANAGEMENT_HPP
