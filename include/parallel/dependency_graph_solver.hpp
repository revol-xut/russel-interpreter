//
// Created by einspaten on 06.03.20.
//

#ifndef TREES_DEPENDENCY_GRAPH_SOLVER_HPP
#define TREES_DEPENDENCY_GRAPH_SOLVER_HPP

#include "graph_constructor.hpp"

class DependencyGraphSolver {
    /*!
     * @brief Algorithm that solves the graph and labels vertices with the execution step they require
     */
public:
    explicit DependencyGraphSolver(const std::shared_ptr<GraphConstructor> &new_ptr);

    ~DependencyGraphSolver();

    void solve();

    auto highest_step_count() -> unsigned int;

private:
    unsigned int highest_step_counter = 0;
    std::shared_ptr<GraphConstructor> tree_;

};


#endif //TREES_DEPENDENCY_GRAPH_SOLVER_HPP
