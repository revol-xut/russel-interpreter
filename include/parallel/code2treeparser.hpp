//
// Created by einspaten on 07.03.20.
//

#ifndef TREES_CODE2TREEPARSER_HPP
#define TREES_CODE2TREEPARSER_HPP

#include <memory>
#include <string>
#include "../routine/routine.hpp"

#include "graph_constructor.hpp"


class Code2TreeParser {
    /*!
     * @brief This Class generates a dependency graph from russel byte code.
     */
public:
    Code2TreeParser(const std::shared_ptr<char[]> &program, unsigned int size);

    explicit Code2TreeParser(const std::shared_ptr<runtime::Routine> &program);

    ~Code2TreeParser();

    void parse();

    auto getTree() -> std::shared_ptr<GraphConstructor>;

private:
    // Unsigned int refers to every line of program code
    std::shared_ptr<GraphConstructor> tree_;

    std::shared_ptr<runtime::Routine> routine_;

};

#endif //TREES_CODE2TREEPARSER_HPP
