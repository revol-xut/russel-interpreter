//
// Created by einspaten on 22.03.20.
//

#ifndef PARSER_GRAPH_CONSTRUCTOR_HPP
#define PARSER_GRAPH_CONSTRUCTOR_HPP

#include <map>
#include "../routine/routine.hpp"
#include "memory_management.hpp"

class GraphConstructor {
    /*!
     * @brief Constructor Class for the Dependency Graph
     */
public:

    GraphConstructor();

    ~GraphConstructor();

    /*!
     * @brief Creates new Vertex which represents given var and was creates by given command
     * @param var Index of variable
     * @param data Command that created this vertex
     * @return id
     */
    auto add_vertex(unsigned int var, runtime::Command data) -> unsigned int;

    /*!
     * @brief Connects to vertices
     * @param index1
     * @param index2
     */
    void connect_vertex(unsigned int index1, unsigned int index2);

    auto get_first_layer_vertices() -> std::vector<std::shared_ptr<DependencyVertex>>;

    auto get_vertices_with_step(unsigned int step) -> std::vector<std::shared_ptr<DependencyVertex>>;

    auto get_index_of_latest_var(unsigned int variable) -> unsigned int;

    auto get_vertex(unsigned int index) -> unsigned int;

    auto generate_passive_vertex(runtime::Command data) -> unsigned int;

    auto get_required_vars() -> std::vector<unsigned int>;

    static unsigned int variable_count_;
private:

    void add_var(unsigned int);

    std::shared_ptr<MemoryManagement> mem_m_;
    std::vector<unsigned int> required_vars_;

    std::map<unsigned int, unsigned int> local_variable_map_ = {};


};


#endif //PARSER_GRAPH_CONSTRUCTOR_HPP
