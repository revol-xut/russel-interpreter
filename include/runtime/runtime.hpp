//
// Created by revol-xut on 28.01.20.
//

#ifndef RUSSEL_INTERPRETER_RUNTIME_HPP
#define RUSSEL_INTERPRETER_RUNTIME_HPP

#include <array>
#include <vector>
#include "../data_classes/abstract_type.hpp"
#include "../routine/routine.hpp"

namespace runtime {
    struct Error {
        std::string message;
        unsigned int error_code;
        unsigned int byte_code_position;
        runtime::Command failed_command;
    };

    constexpr unsigned int kErrorInvalidTyp = 1;
    constexpr unsigned int kErrorOutOfBounds = 2;
    constexpr unsigned int kErrorMemoryOutOfBounds = 10;

    /*!
     * @brief Class contains runtime data like variables
     */
    class Runtime {
    public:
        //TODO: maybe use std::array

        Runtime(const std::shared_ptr<char[]> &data, const std::shared_ptr<char[]> &program, unsigned int size_routine,
                unsigned int data_size);

        Runtime(const std::shared_ptr<Routine> &routine);

        void setData(const std::shared_ptr<char[]> &data, unsigned int data_size);

        ~Runtime();

        auto getVariable(unsigned int index) -> datatypes::AbstractType &;

        void execute();

        void operator()();

        auto getVariableCount() const -> unsigned int;

        auto get_exit_status() -> int;

        auto get_datatype(unsigned int index) -> unsigned int;

        auto get_errors() -> std::vector<std::shared_ptr<Error>>;

    private:

        auto get_size_from_var(unsigned int index) -> unsigned int;

        void add(unsigned int index_1, unsigned int index_2, unsigned int push_on);

        void mul(unsigned int index_1, unsigned int index_2, unsigned int push_on);

        void equals(unsigned int index_1, unsigned int index_2, unsigned int push_on);

        void generate_invalid_typ_error(unsigned int position, runtime::Command command);

        void generate_outof_bounds_error(unsigned int position, runtime::Command command);

        void generate_not_enough_mem_error(unsigned int position, runtime::Command command);

        std::vector<datatypes::AbstractType> variables_;

        unsigned int variable_count_ = 0, byte_read_ = 0, data_field_size_ = 0;

        float exit_status_ = 0;

        std::shared_ptr<char[]> data_;
        std::shared_ptr<Routine> routine_;
        std::vector<std::shared_ptr<Error>> errors_;

    };
}


#endif //RUSSEL_INTERPRETER_RUNTIME_HPP
