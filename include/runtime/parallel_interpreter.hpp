//
// Created by einspaten on 26.03.20.
//

#ifndef RUSSEL_INTERPRETER_PARALLEL_INTERPRETER_HPP
#define RUSSEL_INTERPRETER_PARALLEL_INTERPRETER_HPP

#include <map>
#include <queue>

//#include "task.hpp"
#include "../data_classes/abstract_type.hpp"
#include "../routine/routine.hpp"

class ParallelInterpreter {
public:
    ParallelInterpreter();

    ~ParallelInterpreter();

    //  auto getHighestPriority() -> std::shared_ptr<Task>;

    //  void merge(const std::map<unsigned int, dadatatypes::AbstractType> &variables);

    void read_from_file(const std::string &path);

    void set_data(const std::shared_ptr<char[]> &data);

private:

    void check_and_create_new_tasks();

    void add(unsigned int index_1, unsigned int index_2, unsigned int push_on);

    void mul(unsigned int index_1, unsigned int index_2, unsigned int push_on);

    void equals(unsigned int index_1, unsigned int index_2, unsigned int push_on);

    std::vector<datatypes::AbstractType> variables_;

    unsigned int variable_count_ = 0, byte_read_ = 0;

    float exit_status_ = -10;

    std::shared_ptr<char[]> data_;
    std::shared_ptr<runtime::Routine> routine_;

};


#endif //RUSSEL_INTERPRETER_PARALLEL_INTERPRETER_HPP
