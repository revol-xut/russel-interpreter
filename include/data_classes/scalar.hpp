//
// Created by revol-xut on 25.01.20.
//

#ifndef RUSSEL_INTERPRETER_SCALAR_HPP
#define RUSSEL_INTERPRETER_SCALAR_HPP

#include "matrix.hpp"
#include "vector.hpp"
#include "boolean.hpp"
#include "data_class_interface.hpp"

namespace datatypes {

    class Matrix2D;

    class Vector;

    class Scalar : public datatypes::DataClassInterface {
    public:
        /*!
         * @brief Initializes Scalar value with the given Value
         * @param value
         */
        Scalar(float value);

        void parse(char *data_ptr) override;

        void serialize(char *data_ptr) override;

        auto expected_size() -> unsigned override;

        auto getData() -> defaultType;

        void setData(defaultType value);

        void add(Scalar *other_ptr, Scalar *push_on_ptr);

        void add(Matrix2D *other_ptr, Matrix2D *push_on_ptr);

        void add(Vector *other_ptr, Vector *push_on_ptr);

        void mul(Scalar *other_ptr, Scalar *push_on_ptr);

        void mul(Matrix2D *other_ptr, Matrix2D *push_on_ptr);

        void mul(Vector *other_ptr, Vector *push_on_ptr);

        void equals(Scalar *other_ptr, Boolean *push_on_ptr);

        void print();

    private:

        datatypes::defaultType value_;

    };
}


#endif //RUSSEL_INTERPRETER_SCALAR_HPP
