//
// Created by revol-xut on 28.01.20.
//

#ifndef RUSSEL_INTERPRETER_ABSTRACT_TYPE_HPP
#define RUSSEL_INTERPRETER_ABSTRACT_TYPE_HPP

#include "data_class_interface.hpp"
#include "matrix.hpp"
#include "vector.hpp"
#include "scalar.hpp"


namespace datatypes {
    /*!
     * @brief This Class wraps all data types so the can be stored in one array
     * TODO: Maybe rewrite that thing as a union
     */
    class AbstractType {
    public:
        AbstractType(unsigned int type, void *ptr);

        auto getMatrix2D() -> datatypes::Matrix2D *;

        auto getVector() -> datatypes::Vector *;

        auto getScalar() -> datatypes::Scalar *;

        auto getBoolean() -> datatypes::Boolean *;

        auto getType() const -> unsigned int;

        auto getSize() -> unsigned int;

        void serializeObject(char *data);

    private:

        void *ptr_ = nullptr;
        unsigned int type_ = kNone;
    };
}


#endif //RUSSEL_INTERPRETER_ABSTRACT_TYPE_HPP
