//
// Created by revol-xut on 25.01.20.
//

#ifndef RUSSEL_INTERPRETER_VECTOR_HPP
#define RUSSEL_INTERPRETER_VECTOR_HPP

#include <Eigen/Dense>

#include "data_class_interface.hpp"
#include "matrix.hpp"
#include "scalar.hpp"
#include "boolean.hpp"

namespace datatypes {

    class Matrix2D;

    class Scalar;


    class Vector : public DataClassInterface {
    public:
        Vector(unsigned int size);

        ~Vector();

        void parse(char *data_ptr) override;

        void serialize(char *data_ptr) override;

        auto expected_size() -> unsigned override;

        void add(Scalar *other_ptr, Vector *push_on_ptr);

        void add(Vector *other_ptr, Vector *push_on_ptr);

        // Mathematically undefined behavior interpreted as adding the vector to every column

        void mul(Scalar *other_ptr, Vector *push_on_ptr);

        // Dot (result scalar) / Cross Product ( result vector)
        void cross_mul(Vector *other_ptr, Vector *push_on_ptr);

        void dot_mul(Vector *other_ptr, Scalar *push_on_ptr);

        void mul(Matrix2D *other_ptr, Vector *push_on_ptr);

        void equals(Vector *other_ptr, Boolean *push_on_ptr);

        void setData(const Eigen::VectorXf &new_vector);

        auto getData() -> Eigen::VectorXf &;

        void setDimensions(unsigned int new_dim);

        auto getDimensions() -> unsigned int;

        void print();

    private:
        unsigned int dimensions_;
        std::shared_ptr<Eigen::VectorXf> vector_;
    };

}


#endif //RUSSEL_INTERPRETER_VECTOR_HPP
