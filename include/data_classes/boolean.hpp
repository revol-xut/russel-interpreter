//
// Created by einspaten on 19.02.20.
//

#include "./data_class_interface.hpp"

#ifndef RUSSEL_INTERPRETER_BOOLEAN_HPP
#define RUSSEL_INTERPRETER_BOOLEAN_HPP


namespace datatypes {
    class Boolean : public DataClassInterface {
    public:
        Boolean();

        ~Boolean();

        void parse(char *data_ptr) override;

        void serialize(char *data_ptr) override;

        auto expected_size() -> unsigned int override;
        void equals(Boolean *other_ptr, Boolean *push_on_ptr);
        void negate();
        auto getValue() -> bool;
        void setData(bool value);

    private:

        bool value_;

    };
}


#endif //RUSSEL_INTERPRETER_BOOLEAN_HPP
