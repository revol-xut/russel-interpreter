//
// Created by revol-xut on 25.01.20.
//

#ifndef RUSSEL_INTERPRETER_DATA_CONTAINER_HPP
#define RUSSEL_INTERPRETER_DATA_CONTAINER_HPP

namespace datatypes {


    // This Struct is used for better management of data.
    // So that the typ of a DataClassInterface can be changed rapidly.
    struct DataContainer {
        void *data;
    };


}

#endif //RUSSEL_INTERPRETER_DATA_CONTAINER_HPP
