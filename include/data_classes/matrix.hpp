//
// Created by revol-xut on 24.01.20.
//

#ifndef RUSSEL_INTERPRETER_MATRIX_HPP
#define RUSSEL_INTERPRETER_MATRIX_HPP

#include <Eigen/Dense>
#include "scalar.hpp"
#include "vector.hpp"
#include "boolean.hpp"
#include "data_class_interface.hpp"


namespace datatypes {

    class Scalar;

    class Vector;


    class Matrix2D : public DataClassInterface {
    public:
        /*!
         * @brief Creates 2D Matrix with size_x columns and size_y rows
         * @param size_x columns
         * @param size_y columns
         */
        Matrix2D(unsigned cols, unsigned rows);

        /*!
         * @brief Destructor
         */
        ~Matrix2D();

        /*!
         * @brief adds other matrix and pushes result onto the second pointer
         * @param other_ptr other matrix
         * @param push_on_ptr where the result should be mut
         */
        void add(Matrix2D *other_ptr, Matrix2D *push_on_ptr);

        //  void add(const std::shared_ptr<Vector>& other_ptr, const std::shared_ptr<Vector>& push_on_ptr);

        /*!
         * @brief Increases every value in matrix by given scalar.
         * @param other_ptr scalar value
         * @param push_on_ptr result pointer
         */
        void add(Scalar *other_ptr, Matrix2D *push_on_ptr);

        /*!
         * @brief multiplies those to matrices
         * @param other_ptr matrix
         * @param push_on_ptr result pointer
         */
        void mul(Matrix2D *other_ptr, Matrix2D *push_on_ptr);

        /*!
         * @brief multiplies matrix with vector
         * @param other_ptr
         * @param push_on_ptr
         */
        void mul(Vector *other_ptr, Vector *push_on_ptr);

        /*!
         * @brief multiplies matrix with given scalar
         * @param other_ptr scalar value
         * @param push_on_ptr result pointer
         */
        void mul(Scalar *other_ptr, Matrix2D *push_on_ptr);


        void equals(Matrix2D *other_ptr, Boolean *push_on_ptr);

        //void pot(const std::shared_ptr<DataClassInterface>& other_ptr, const std::shared_ptr<DataClassInterface>& push_on_ptr);
        /*!
         * @brief Generates Matrix Contents from given char pointer. The pointer is expected to be already validated.
         * @param data_ptr char pointer pointing to contents float values.
         */
        void parse(char *data_ptr) override;

        /*!
         * @brief Returns size byte field for the contents of this matrix
         * @return
         */

        void serialize(char *data_ptr) override;

        auto expected_size() -> unsigned override;

        /*!
         * @brief Returns Eigen Object
         * @return
         */
        auto getData() -> const Eigen::MatrixXf &;

        void setSize(unsigned int size_x, unsigned int size_y);

        auto getSizeX() const -> unsigned int;

        auto getSizeY() const -> unsigned int;

        /*!
         * @brief Sets Eigen Matrix object
         * @param eigen_matrix
         */
        void setData(const Eigen::MatrixXf &eigen_matrix);

    private:

        unsigned cols_, rows_;

        //TODO: Eigen Documentation recommends fixed size metrices because of performance increase
        std::shared_ptr<Eigen::MatrixXf> eigen_matrix_;
    };
}


#endif //RUSSEL_INTERPRETER_MATRIX_HPP
