//
// Created by revol-xut on 24.01.20.
//

#ifndef RUSSEL_INTERPRETER_DATA_CLASS_INTERFACE_HPP
#define RUSSEL_INTERPRETER_DATA_CLASS_INTERFACE_HPP

#include <memory>
#include <stdlib.h>
#include <cmath>
#include <functional>

#include "data_container.hpp"

namespace datatypes {

    constexpr unsigned kNone = 0;
    constexpr unsigned kByte = 1;
    constexpr unsigned kNumeric = 2;
    constexpr unsigned kVector = 3;
    constexpr unsigned kMatrix2D = 4;
    constexpr unsigned kMatrix3D = 5;
    constexpr unsigned kBoolean = 6;

    // Expecting that float has a fixed bit size: https://stackoverflow.com/questions/1331821/fixed-width-floating-point-numbers-in-c-c
    typedef float defaultType;

    // Using C-Raw Pointer is a code smell but the most efficient thing here because this function will be called VERY often
    auto castToValue(const char *) -> defaultType;

    void degrade(defaultType value, char *return_val);

    class DataClassInterface {
    public:
        /*!
         * @brief Parses Object from char field. Char field is expected to be already validated.
         * @param data_ptr Pointer to byte field
         */
        virtual void parse(char *data_ptr) = 0;

        /*!
         * @brief Serializes this object and puts it onto the char pointer. Char field is expected to be already allocated
         * @param data_ptr Pointer to byte field
         */
        virtual void serialize(char *data_ptr) = 0;

        /*!
         * @brief Returns the size of byte field this object needs
         * @return size
         */
        virtual auto expected_size() -> unsigned = 0;
        /*!
         * @brief Type of this Object
         * @return
         */
        auto getType() -> unsigned {
            return type_;
        }

        /*!
         * @brief set Type of this Object
         * @param type
         */
        void setType(unsigned type) {
            type_ = type;
        }

    private:

        unsigned type_ = kNone;

    };
}

#endif //RUSSEL_INTERPRETER_DATA_CLASS_INTERFACE_HPP
